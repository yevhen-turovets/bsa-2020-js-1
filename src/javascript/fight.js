import { randomInteger } from './helpers/apiHelper.js';

export function fight(firstFighter, secondFighter) {
	let healthFirstFighter = firstFighter.health;
  let healthSecondFighter = secondFighter.health;

	const hit = function* () {
		yield healthSecondFighter -= getDamage(firstFighter, secondFighter);
		yield healthFirstFighter -= getDamage(secondFighter, firstFighter);
	};

  for (;;) {
  	let hitResult =  hit();

  	if (hitResult.next().value <= 0) {
  		return (firstFighter);
  	} else if (hitResult.next().value <= 0) {
  		return (secondFighter);
  	}
  }
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker)-getBlockPower(enemy);
  
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack*randomInteger(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense*randomInteger(1, 2);
}
