import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { createModalName, createModalImage } from './fighterDetails.js';

export function showWinnerModal(fighter) {
	const title = 'Fighter winner';
  const bodyElement = createFighterWinner(fighter);
  showModal({ title, bodyElement });
}

function createFighterWinner(fighter) {
  const fighterWinnerBlock = createElement({ tagName: 'div', className: 'modal-body' });
  const image = createModalImage(fighter.source);
  const name = createModalName(fighter.name);

  fighterWinnerBlock.append(image, name);

  return fighterWinnerBlock;
}
