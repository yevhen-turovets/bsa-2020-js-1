import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const maxHealth = 70,
        maxAttack = 7,
        maxDefense = 7,
        healthClassname = "fighter-health",
        attackClassname = "fighter-attack",
        defenseClassname = "fighter-defense";
  const fighterDetailsBlock = createElement({ tagName: 'div', className: 'modal-body' });

  const image = createModalImage(fighter.source);
  const name = createModalName(fighter.name);
  const health = createModalCharacteristic(fighter.health, healthClassname, maxHealth);
  const attack = createModalCharacteristic(fighter.attack, attackClassname, maxAttack);
  const defense = createModalCharacteristic(fighter.defense, defenseClassname, maxDefense);

  fighterDetailsBlock.append(image, name, health, attack, defense);

  return fighterDetailsBlock;
}

export function createModalImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  
  return imgElement;
}

export function createModalName(name) {
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  nameElement.innerText = name;
  
  return nameElement;
}

function createModalCharacteristic(characteristic, blockClass, maxValue) {
  const characteristicBlock = createElement({ tagName: 'div', className: 'fighter-characteristic' });
  const characteristicElement = createElement({ tagName: 'div', className: blockClass });

  characteristicElement.innerText = characteristic;
  characteristicBlock.append(characteristicElement);
  characteristicBlock.append(createProgressbar(characteristic, maxValue));
  
  return characteristicBlock;
}

function createProgressbar(value, max) {
  const attributes = { value: value, max: max };

  return createElement({ tagName: 'progress', className: 'progressbar', attributes });
}
